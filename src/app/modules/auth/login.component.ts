import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { catchError, Observable, of, take, tap } from 'rxjs';

import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  signal,
  WritableSignal,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

import { AccountModel } from '../../models/account.model';

import { AuthService } from './services/auth.service';

@Component({
  selector: 'sezar-source-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [InputTextModule, ButtonModule, ReactiveFormsModule, NgIf],
  providers: [AuthService],
})
export class LoginComponent {
  loginForm!: FormGroup;
  errorMessage: WritableSignal<string | null> = signal(null);

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _authService: AuthService,
  ) {
    this.loginForm = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
    localStorage.clear();
  }

  public login(): void {
    const userInputs: AccountModel = {
      email: this.loginForm.value['email'],
      password: this.loginForm.value['password'],
    };

    this._authService
      .login(userInputs)
      .pipe(
        tap((authObject: { token: string }) => {
          localStorage.setItem(
            'user',
            JSON.stringify({
              email: this.loginForm.value['email'],
              token: authObject.token,
            }),
          );
          this._router.navigate(['']);
        }),
        catchError((error: any): Observable<any> => {
          this.errorMessage.set(JSON.stringify(error.error));
          return of(error);
        }),
        take(1),
      )
      .subscribe();
  }
}

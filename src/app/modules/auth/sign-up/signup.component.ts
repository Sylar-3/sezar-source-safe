import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { catchError, Observable, of, take, tap } from 'rxjs';

import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  signal,
  WritableSignal,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

import { AccountModel } from '../../../models/account.model';
import { GroupService } from '../../../shared/services/group.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'sezar-source-signup',
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.scss',
  changeDetection: ChangeDetectionStrategy.Default,
  standalone: true,
  imports: [InputTextModule, ButtonModule, ReactiveFormsModule, NgIf],
  providers: [AuthService, GroupService],
})
export class SignupComponent {
  signUpForm!: FormGroup;
  errorMessage: WritableSignal<string | null> = signal(null);

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private readonly _authService: AuthService,
    private readonly _groupService: GroupService,
  ) {
    this.signUpForm = this._fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirm: ['', Validators.required],
    });
    localStorage.clear();
  }

  public login(): void {
    const userInputs: any = {
      name: this.signUpForm.value['name'],
      email: this.signUpForm.value['email'],
      password: this.signUpForm.value['password'],
    };

    const userAcc: AccountModel = {
      email: this.signUpForm.value['email'],
      password: this.signUpForm.value['password'],
    };

    this._authService
      .signUp(userInputs)
      .pipe(
        tap(() => {
          this._authService
            .login(userAcc)
            .pipe(
              tap((authObject: { token: string }) => {
                localStorage.setItem(
                  'user',
                  JSON.stringify({
                    email: this.signUpForm.value['email'],
                    token: authObject.token,
                  }),
                );
                this._groupService
                  .createGroup(this.signUpForm.value['name'])
                  .pipe(
                    tap((group: { group: { id: number; name: string } }) => {
                      this._groupService
                        .joinGroup(group.group.id)
                        .pipe(take(1))
                        .subscribe();
                    }),
                    take(1),
                  )
                  .subscribe();
                this._router.navigate(['']);
              }),

              take(1),
            )
            .subscribe();
        }),
        catchError((error: any): Observable<any> => {
          this.errorMessage.set(JSON.stringify(error.error));
          return of(error);
        }),
        take(1),
      )
      .subscribe();
  }

  public inputValid(): boolean {
    if (
      this.signUpForm.controls['password'].value ===
        this.signUpForm.controls['confirm'].value &&
      this.signUpForm.valid
    ) {
      return true;
    } else {
      return false;
    }
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { AccountModel } from '../../../models/account.model';

@Injectable()
export class AuthService {
  private readonly AUTH_ENDPOINTS = {
    login: 'signin',
    signUp: 'signup',
  };

  constructor(private readonly _http: HttpClient) {}

  public login(account: AccountModel): Observable<any> {
    const body: AccountModel = {
      email: account.email,
      password: account.password,
    };

    return this._http.post<any>(
      environment.CURRENT_DOMAIN + this.AUTH_ENDPOINTS.login,
      body,
    );
  }

  public signUp(account: {
    name: string;
    email: string;
    password: string;
  }): Observable<any> {
    const body: { name: string; email: string; password: string } = {
      name: account.name,
      email: account.email,
      password: account.password,
    };

    return this._http.post<any>(
      environment.CURRENT_DOMAIN + this.AUTH_ENDPOINTS.signUp,
      body,
    );
  }
}

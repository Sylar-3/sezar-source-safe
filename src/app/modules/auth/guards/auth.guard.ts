import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  constructor(public router: Router) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    // Guard for user is login or not
    const userLocalStorage: string | null = localStorage.getItem('user');

    if (!userLocalStorage) {
      this.router.navigate(['auth']);
      return false;
    } else {
      const user = JSON.parse(userLocalStorage);
      if (!user || user === null || user === '') {
        this.router.navigate(['auth']);
        return false;
      } else if (user) {
        if (!Object.keys(user).length) {
          this.router.navigate(['auth']);
          return false;
        }
      }
    }
    return true;
  }
}

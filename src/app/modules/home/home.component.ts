import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { catchError, Observable, of, take, tap } from 'rxjs';

import { DatePipe, NgFor, NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  signal,
  WritableSignal,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

import { LogModel } from 'src/app/models/log.model';

import { GroupsListResponse, OneGroupResponse } from '../../models/group.model';
import { CardComponent } from '../../shared/components/card/card.component';
import { GroupService } from '../../shared/services/group.service';

import { FileService } from './services/files.service';

@Component({
  selector: 'sezar-source-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  changeDetection: ChangeDetectionStrategy.Default,
  standalone: true,
  imports: [
    NgFor,
    NgIf,
    CardComponent,
    ButtonModule,
    ReactiveFormsModule,
    InputTextModule,
    DatePipe,
    FormsModule,
  ],
  providers: [GroupService, FileService],
})
export class HomeComponent implements OnInit {
  groups: WritableSignal<boolean> = signal(false);
  joinGroups: WritableSignal<boolean> = signal(false);
  uploadDialogOpen: WritableSignal<boolean> = signal(false);
  currentFileGroup: WritableSignal<number> = signal(0);

  selectionVisible: boolean = false;

  logDialogOpen: WritableSignal<boolean> = signal(false);
  log: WritableSignal<
    Array<{
      id: number;
      file_id: number;
      user_id: number;
      changes: string;
      created_at: string;
      updated_at: string;
    }>
  > = signal([]);

  groupForm!: FormGroup;
  joinGroupForm!: FormGroup;
  uploadFileForm!: FormGroup;

  checkInFilesList: Array<number> = [];
  checkOut: Array<number> = [];

  groupsList: WritableSignal<
    Array<{
      id: number;
      name: string;
    }>
  > = signal([]);

  myGroups: WritableSignal<
    Array<{
      id: number;
      name: string;
      files: Array<{
        id: 1;
        name: string;
        user_id: number;
        group_id: number;
        status: string; //TODO make status enum
      }>;
    }>
  > = signal([]);

  userEmail!: string;

  constructor(
    private readonly _fb: FormBuilder,
    private readonly _router: Router,
    private readonly _groupService: GroupService,
    private readonly _cdr: ChangeDetectorRef,
    private readonly _fileService: FileService,
  ) {
    this.groupForm = this._fb.group({
      name: ['', Validators.required],
    });

    this.joinGroupForm = this._fb.group({
      joinControl: ['', Validators.required],
    });

    this.uploadFileForm = this._fb.nonNullable.group({
      fileName: this._fb.nonNullable.control<string>('', [Validators.required]),
      file: this._fb.nonNullable.control<File | null>(
        null,
        Validators.required,
      ),
    });

    this.userEmail = JSON.parse(localStorage.getItem('user') ?? '').email;
  }

  ngOnInit(): void {
    this.getAllGroups();
  }

  public showGroupDialog(): void {
    this.groups.set(true);
  }

  public showGroupJoinDialog(): void {
    this.joinGroups.set(true);
  }

  public showUploadDialog(groupId: number): void {
    this.uploadDialogOpen.set(true);
    this.currentFileGroup.set(groupId);
  }

  public closeDialog(): void {
    this.groups.set(false);
    this.joinGroups.set(false);
    this.uploadDialogOpen.set(false);
    this.logDialogOpen.set(false);
  }

  public onUpload(event: any): void {
    if (event.target.files && event.target.files.length > 0) {
      this.uploadFileForm.controls['file'].setValue(event.target.files[0]);
    }
  }

  public submitFile(): void {
    this._fileService
      .uploadFile(
        this.uploadFileForm.controls['file'].value,
        this.uploadFileForm.controls['fileName'].value,
        this.currentFileGroup(),
      )
      .pipe(
        tap(() => {
          this.getAllGroups();
        }),
        take(1),
      )
      .subscribe();
    this.closeDialog();
  }

  public updated(): void {
    this.getAllGroups();
  }

  public createGroup(): void {
    this._groupService
      .createGroup(this.groupForm.controls['name'].value)
      .pipe(
        tap(
          (group: {
            group: {
              id: number;
              name: string;
            };
          }) => {
            this._groupService
              .joinGroup(group.group.id)
              .pipe(
                take(1),
                tap(() => {
                  this.getAllGroups();
                }),
              )
              .subscribe();
          },
        ),
        take(1),
      )
      .subscribe();
    this.closeDialog();
  }

  public joinGroup(): void {
    this._groupService
      .joinGroup(this.joinGroupForm.controls['joinControl'].value.id)
      .pipe(
        tap(() => {
          this.getAllGroups();
        }),
        take(1),
      )
      .subscribe();
    this.closeDialog();
  }

  private getAllGroups(): void {
    this._groupService
      .getAllGroups()
      .pipe(
        tap((groupsList: GroupsListResponse) => {
          this.groupsList.set(groupsList.groups);
          this.myGroups.set([]);
          this.groupsList().forEach((group: { id: number; name: string }) => {
            this.getGroupById(group.id);
          });
        }),
        take(1),
      )
      .subscribe();
  }

  private getGroupById(groupId: number): void {
    this._groupService
      .getGroupById(groupId)
      .pipe(
        tap((group: OneGroupResponse) => {
          for (const user of group.group.users) {
            if (user.email === this.userEmail) {
              this.myGroups().push({
                id: group.group.id,
                name: group.group.name,
                files: group.group.files,
              });
              this._cdr.detectChanges();
              break;
            }
          }
        }),
        take(1),
      )
      .subscribe();
  }

  public getLog(): void {
    this.logDialogOpen.set(true);
    this._fileService
      .getLog()
      .pipe(
        tap((changes: LogModel) => {
          this.log.set(changes.updated);
        }),
        take(1),
      )
      .subscribe();
  }

  public logout(): void {
    localStorage.clear();
    this._router.navigate(['auth']);
  }

  public bulkCheckIn(): void {
    this._fileService
      .bulkCheckIn(this.checkInFilesList)
      .pipe(
        tap(() => {
          this._cdr.detectChanges();
        }),
        catchError((): Observable<Array<number>> => {
          this.getAllGroups();
          this.checkInFilesList = [];
          return of([]);
        }),
        take(1),
      )
      .subscribe();
  }

  public bulkCheckOut(): void {
    this.checkOut = [];
    this.myGroups().forEach((group: any) => {
      group.files.forEach((file: any) => {
        if (file.status === 'locked') {
          this.checkOut.push(file.id);
        }
      });
    });
    this._fileService
      .bulkCheckOut(this.checkOut)
      .pipe(
        take(1),
        tap(() => {
          this.getAllGroups();
        }),
      )
      .subscribe();
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
import { filter, map, Observable } from 'rxjs';

import {
  HttpClient,
  HttpEventType,
  HttpHeaders,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { LogModel } from 'src/app/models/log.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class FileService {
  private readonly FILE_ENDPOINTS = {
    files: 'files',
    lock: 'lock',
    free: 'free',

    download: 'download',

    lockAll: 'files/lock_all',
    freeAll: 'files/free_all',

    log: 'changes',
  };

  constructor(private readonly _http: HttpClient) {}

  public uploadFile(
    file: File,
    fileName: string,
    groupId: number,
  ): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    const body: any = {
      file: file,
      name: fileName,
      group_id: groupId,
    };

    const formData: FormData = new FormData();
    formData.append('file', body.file, body.file.name);
    formData.append('name', body.name);
    formData.append('group_id', body.group_id.toString());

    return this._http.post<any>(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.files}`,
      formData,
      { headers: headers },
    );
  }

  public bulkCheckOut(filesIds: Array<number>): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.post(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.freeAll}`,
      { ids: filesIds },
      { headers: headers },
    );
  }

  public getLog(): Observable<LogModel> {
    return this._http.get<LogModel>(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.log}`,
    );
  }

  public checkIn(fileId: number): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.put<any>(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.files}/${fileId}/${this.FILE_ENDPOINTS.lock}`,
      {},
      { headers: headers },
    );
  }

  public bulkCheckIn(fileId: Array<number>): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.put<any>(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.files}/${this.FILE_ENDPOINTS.lockAll}`,
      { ids: fileId },
      { headers: headers },
    );
  }

  public checkOut(fileId: number): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.put<any>(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.files}/${fileId}/${this.FILE_ENDPOINTS.free}`,
      {},
      { headers: headers },
    );
  }

  public download(fileId: number): Observable<HttpResponse<Blob>> {
    const userObject: any = JSON.parse(localStorage.getItem('user') || '{}');

    const headers: HttpHeaders = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    const options: any = {
      headers: headers,
      observe: 'events' as const,
      responseType: 'blob' as const,
    };

    return this._http
      .get(
        `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.files}/${fileId}/${this.FILE_ENDPOINTS.download}`,
        options,
      )
      .pipe(
        filter((event: any) => event.type === HttpEventType.Response),
        map((event: any) => event as HttpResponse<Blob>),
      );
  }

  public update(fileId: number, file: File): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') || '{}');
    const headers: HttpHeaders = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    const body: any = {
      file: file,
    };

    const formData: FormData = new FormData();
    formData.append('file', body.file, body.file.name);
    formData.append('_method', 'PUT');

    return this._http.post(
      `${environment.CURRENT_DOMAIN}${this.FILE_ENDPOINTS.files}/${fileId}`,
      formData,
      { headers: headers },
    );
  }
}

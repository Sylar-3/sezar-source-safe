/* eslint-disable @typescript-eslint/no-explicit-any */
import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { GroupsListResponse, OneGroupResponse } from '../../models/group.model';

@Injectable()
export class GroupService {
  private readonly GROUPS_ENDPOINTS = {
    group: 'groups',
    joinGroup: 'join',
  };

  constructor(private readonly _http: HttpClient) {}

  public createGroup(groupName: string): Observable<{
    group: {
      id: number;
      name: string;
    };
  }> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    const body: any = {
      name: groupName,
    };

    return this._http.post<{
      group: {
        id: number;
        name: string;
      };
    }>(environment.CURRENT_DOMAIN + this.GROUPS_ENDPOINTS.group, body, {
      headers: headers,
    });
  }

  public joinGroup(groupId: number): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.post<any>(
      environment.CURRENT_DOMAIN +
        this.GROUPS_ENDPOINTS.group +
        `/${groupId}/` +
        this.GROUPS_ENDPOINTS.joinGroup,
      {},
      {
        headers: headers,
      },
    );
  }

  public getAllGroups(): Observable<GroupsListResponse> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.get<GroupsListResponse>(
      environment.CURRENT_DOMAIN + this.GROUPS_ENDPOINTS.group,
      { headers: headers },
    );
  }

  public getGroupById(groupId: number): Observable<OneGroupResponse> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.get<OneGroupResponse>(
      environment.CURRENT_DOMAIN + this.GROUPS_ENDPOINTS.group + `/${groupId}`,
      { headers: headers },
    );
  }

  public deleteGroup(groupId: number): Observable<any> {
    const userObject: any = JSON.parse(localStorage.getItem('user') ?? '');
    const headers: any = new HttpHeaders().append(
      'Authorization',
      `Bearer ${userObject.token}`,
    );

    return this._http.delete<any>(
      environment.CURRENT_DOMAIN + this.GROUPS_ENDPOINTS.group + `/${groupId}`,
      { headers: headers },
    );
  }
}

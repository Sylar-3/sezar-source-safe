/* eslint-disable @typescript-eslint/typedef */
import { take, tap } from 'rxjs';

import { NgClass, NgIf } from '@angular/common';
import { HttpEventType } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  signal,
  WritableSignal,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

import { FileService } from 'src/app/modules/home/services/files.service';

@Component({
  selector: 'sezar-source-card',
  templateUrl: './card.component.html',
  styleUrl: './card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgIf, NgClass, ReactiveFormsModule],
  providers: [FileService],
})
export class CardComponent {
  @Input({ required: true }) fileId!: number;
  @Input({ required: true }) title!: string;
  @Input({ required: true }) content!: string;

  @Output() updateEvent = new EventEmitter<boolean>();

  updateDialogOpen: WritableSignal<boolean> = signal(false);
  updateFileForm!: FormGroup;

  constructor(
    private readonly _fileService: FileService,
    private readonly _cdr: ChangeDetectorRef,
    private readonly _fb: FormBuilder,
  ) {
    this.updateFileForm = this._fb.nonNullable.group({
      file: this._fb.nonNullable.control<File | null>(
        null,
        Validators.required,
      ),
    });
  }

  public showUpdateDialog(): void {
    this.updateDialogOpen.set(true);
  }

  public closeDialog(): void {
    this.updateDialogOpen.set(false);
  }

  public onUpload(event: any): void {
    if (event.target.files && event.target.files.length > 0) {
      this.updateFileForm.controls['file'].setValue(event.target.files[0]);
    }
  }

  public checkIn(): void {
    this._fileService
      .checkIn(this.fileId)
      .pipe(
        tap(() => {
          this.content = 'locked';
          this._cdr.detectChanges();
          setTimeout(() => {
            this.checkOut();
          }, 10000);
        }),
        take(1),
      )
      .subscribe();
  }

  public checkOut(): void {
    this._fileService
      .checkOut(this.fileId)
      .pipe(
        tap(() => {
          this.content = 'free';
          this._cdr.detectChanges();
        }),
        take(1),
      )
      .subscribe();
  }

  public download(): void {
    this._fileService
      .download(this.fileId)
      .pipe(
        tap((event: any) => {
          if (event.type === HttpEventType.Response) {
            this.saveFile(event.body, event.headers);
          }
        }),
        take(1),
      )
      .subscribe();
  }

  private saveFile(blob: Blob | null, headers: any): void {
    if (!blob) {
      console.error('Empty or null response received.');
      return;
    }

    const contentDisposition = headers.get('content-disposition');
    const fileNameMatch =
      contentDisposition && contentDisposition.match(/filename="(.+)"$/);

    const fileName = fileNameMatch ? fileNameMatch[1] : 'downloaded-file';
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
  }

  public submitFile(): void {
    this._fileService
      .update(this.fileId, this.updateFileForm.controls['file'].value)
      .pipe(
        tap(() => {
          this.updateEvent.emit(true);
        }),
        take(1),
      )
      .subscribe();
    this.closeDialog();
  }
}

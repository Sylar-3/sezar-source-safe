export interface FileModel {
  title: string;
  content: string;
}

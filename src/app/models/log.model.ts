export class LogModel {
  updated!: Array<{
    id: number;
    file_id: number;
    user_id: number;
    changes: string;
    created_at: string;
    updated_at: string;
  }>;
}

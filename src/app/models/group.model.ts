import { FileModel } from './file.model';

export interface GroupModel {
  name: string;
  files: Array<FileModel>;
}

export class GroupsListResponse {
  groups!: Array<{
    id: number;
    name: string;
  }>;
}

export class OneGroupResponse {
  group!: {
    id: number;
    name: string;
    users: Array<{
      id: number;
      name: string;
      email: string;
    }>;
    files: Array<{
      id: 1;
      name: string;
      user_id: number;
      group_id: number;
      status: string; //TODO make status enum
    }>;
  };
}

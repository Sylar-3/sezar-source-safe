/* eslint-disable @typescript-eslint/typedef */
import { Route } from '@angular/router';

import { AuthGuard } from './modules/auth/guards/auth.guard';

export const routes: Route[] = [
  {
    path: 'auth',
    loadComponent: () =>
      import('./modules/auth/login.component').then((c) => c.LoginComponent),
  },
  {
    path: 'signup',
    loadComponent: () =>
      import('./modules/auth/sign-up/signup.component').then(
        (c) => c.SignupComponent,
      ),
  },
  {
    path: '',
    canActivate: [AuthGuard],
    loadComponent: () =>
      import('./modules/home/home.component').then((c) => c.HomeComponent),
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
